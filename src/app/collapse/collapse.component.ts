import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'collapse',
  templateUrl: './collapse.component.html',
  styleUrls: ['./collapse.component.css']
})
export class CollapseComponent implements OnInit {

  public collapse: boolean = false;
  public current: number = 0; 

  @Input() item: any;
  @Input() index: number;

  constructor() { }

  ngOnInit() { }

  toogle(current: number) {
    this.collapse = !this.collapse;
    this.current = current;
  }

  icon(index: number){
    let icon = "+";
    if(index == this.current && this.collapse) {
      icon = " - "; 
    } 
    return icon;
  }
}
